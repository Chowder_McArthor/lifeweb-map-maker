//Defines


world
	mob = /mob/architect
	turf = /turf/floor/natural/dirt
	area = /area
	view = "21x15"
	cache_lifespan = 1
	fps = 25

mob
	step_size = 32

obj
	step_size = 32
	var/rotate = 0 //if it can be rotated with MMB
	var/alt = null //Set this to the alternate icon state - done with alt click

#define MAX_HEIGHT	1
#define MIN_HEIGHT	6
#define ALLDIRS	list(1,2,4,5,6,8,9,10)
/var/global/list/BadFloorPaths = list(/turf/floor, /turf/floor/natural)
/var/global/list/BadWallPaths = list(/turf/wall, /turf/wall/rock/caves)
/var/global/list/BadStructurePaths = list(/obj/structure, /obj/structure/door, /obj/structure/sign, /obj/structure/chair)
