/mob/architect
	name = "architect"
	desc = "An architect of the world."
	icon = 'icons/architect.dmi'
	icon_state = "architect"
	density = 0
	alpha = 150
	var/icons = list("architect","fire","angel","graga","fox","puppeteer","lupine","stalin","dalek","drone")
	var/Atom

	New()
		..()

		spawn(10)

		world << "[name] has joined."

		spawn(10)

		usr << "<br>Controls: <br>\
			Click: Build <br>\
			Right Click: Delete <br>\
			Middle Mouse Button: Rotate (if applicable) <br>\
			Alt-Click: Set alternate icon state"

		icon_state = pick(icons)