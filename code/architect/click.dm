/client/Click(object,location,control,params)
	params=params2list(params)
	if(params["alt"])
		if(istype(object, /obj))
			var/obj/O = object
			if(O.alt)
				if(O.icon_state == O.alt)
					O.icon_state = initial(O.icon_state)
				else
					O.icon_state = O.alt
		return
	if(params["right"])
		if(istype(usr, /mob/architect))
			del(object)
	if(params["left"])
		if(istype(usr, /mob/architect))
			var/mob/architect/M = usr
			var/atom/A = object
			var/turf/T = location
			if(istype(A, /turf))
				if(T && M.Atom)
					if(T == M)
						return
					T = new M.Atom(T)
	if(params["middle"])
		if(istype(object, /obj))
			var/obj/O = object
			if(O.rotate)
				O.dir = turn(O.dir, 90)
