/mob/architect/verb/buildfloor()
	set category = "Building"
	set name = "Build Floor"

	var/list/L = typesof(/turf/floor) - BadFloorPaths
	if(usr)
		Atom = input("Please choose a new floor type.","architect",) in L

/mob/architect/verb/buildwall()
	set category = "Building"
	set name = "Build Wall"

	var/list/L = typesof(/turf/wall) - BadWallPaths
	if(usr)
		Atom = input("Please choose a new wall type.","architect",) in L
/*
/mob/architect/verb/buildstructure()
	set category = "Building"
	set name = "Build Structure"

	var/list/L = typesof(/obj/structure) - BadStructurePaths - typesof(/obj/structure/blacksmith) - typesof(/obj/structure/bed) - typesof(/obj/structure/door) - typesof(/obj/structure/grille) - typesof(/obj/structure/chair)
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L
*/
/mob/architect/verb/buildforge()
	set category = "Building"
	set name = "Build Blacksmith Structures"

	var/list/L = typesof(/obj/structure/blacksmith) - /obj/structure/blacksmith
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

/mob/architect/verb/buildbed()
	set category = "Building"
	set name = "Build Bed"

	var/list/L = typesof(/obj/structure/bed)
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L


/mob/architect/verb/buildgrille()
	set category = "Building"
	set name = "Build Grille"

	var/list/L = typesof(/obj/structure/grille)
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

/mob/architect/verb/builddoor()
	set category = "Building"
	set name = "Build Door"

	var/list/L = typesof(/obj/structure/door) - /obj/structure/door
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

/mob/architect/verb/buildchair()
	set category = "Building"
	set name = "Build Chair"
	var/list/L = typesof(/obj/structure/chair)
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

/mob/architect/verb/buildcloset()
	set category = "Building"
	set name = "Build Closet"
	var/list/L = typesof(/obj/structure/storage/closet) - typesof(/obj/structure/storage/closet/cabinet)
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

/mob/architect/verb/buildcabinet()
	set category = "Building"
	set name = "Build Cabinet"
	var/list/L = typesof(/obj/structure/storage/closet/cabinet)
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

/mob/architect/verb/buildmultiz()
	set category = "Building"
	set name = "Build Ladders and Stairs"
	var/list/L = typesof(/obj/multiz) - /obj/multiz
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

/mob/architect/verb/buildoldways()
	set category = "Building"
	set name = "Build Old Ways Structures"
	var/list/L = typesof(/obj/structure/oldways) - /obj/structure/oldways
	if(usr)
		Atom = input("Please choose a new structure type.","architect",) in L

