/turf/floor/natural
	icon = 'icons/turfs/floors/dirt.dmi'
	var/RandomDir = 0

	New()
		..()
		if(RandomDir)
			RanDir()

/turf/floor/natural/proc/RanDir()
	dir = rand(1,4)

/turf/floor/natural/dirt
	name = "dirt"
	icon_state = "asteroid0"

	New()
		..()
		icon_state = "asteroid[rand(0,13)]"


/turf/floor/natural/surface
	name = "surface"
	icon_state = "surface"
	RandomDir = 1

/turf/floor/natural/seabed
	name = "dirt"
	icon_state = "seabed"
	RandomDir = 1