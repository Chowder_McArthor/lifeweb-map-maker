/turf/floor
	name = "floor"

/turf/floor/wood
	name = "wooden floor"
	icon = 'icons/turfs/floors/wood.dmi'
	icon_state = "wood1"

	New()
		if(prob(40))
			icon_state = "wood[rand(1,3)]"

/turf/floor/stone
	icon = 'icons/turfs/floors/stone.dmi'
	icon_state = "stonefloor"
	New()
		..()
		dir = pick(1,2,4,5,6,8,9,10)
/turf/floor/stone/surgery
	icon_state = "surgery"
	New()
		..()

/turf/floor/stone/surgery/alt
	icon_state = "surgery2"

/turf/floor/stone/bar
	icon_state = "bar"

	New()
		..()
		if(prob(15))
			icon_state = "bar2"

/turf/floor/stone/crafted
	icon_state = "stonecrafted"

/turf/floor/stone/chisled
	icon_state = "newstone"

/turf/floor/stone/brothel
	icon_state = "brothel"