/turf/wall
	name = "wall"
	density = 1

	New()
		..()
		smoothing()

/turf/wall/stone
	name = "stone wall"
	icon_state = "wall0"
	icon = 'icons/turfs/walls/stone.dmi'

/turf/wall/rock
	name = "rock"
	icon_state = "rock"
	icon = 'icons/turfs/walls/rock.dmi'

/turf/wall/rock/caves
	New()
		return

/turf/wall/metal
	name = "metal wall"
	icon = 'icons/turfs/walls/metal.dmi'
	icon_state = "wall"

/turf/wall/rough_metal
	name = "metal wall"
	icon = 'icons/turfs/walls/roughmetal.dmi'
	icon_state = "rwall"
/*
/turf/wall/wood
	name = "wooden wall"
	icon = 'icons/turfs/walls/wood.dmi'
/turf/wall/witch
	icon = 'icons/turfs/walls/witch.dmi'

/turf/wall/strong
	name = "strong wall"
*/


/turf/wall/proc/smoothing()
	overlays.Cut()
	var/junction = 0
	for(var/turf/wall/W in range(1,src))
		if(W.icon == icon)
			if(abs(src.x-W.x)-abs(src.y-W.y))
				junction |= get_dir(src,W)
		W.relativesmooth()
	icon_state = "wall[junction]"

	return

/turf/wall/proc/relativesmooth()
	overlays.Cut()
	var/junction = 0
	for(var/turf/wall/W in range(1,src))
		if(W.icon == icon)
			if(abs(src.x-W.x)-abs(src.y-W.y))
				junction |= get_dir(src,W)
	icon_state = "wall[junction]"


	return
/*
/turf/wall/Del()
	var/temploc = src.loc
	spawn(10)
		for(var/turf/wall/W in range(temploc, 1))
			W.relativesmooth()
	..()*/