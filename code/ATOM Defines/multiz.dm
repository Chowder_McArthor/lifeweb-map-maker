/obj/multiz
	icon = 'icons/turfs/multiz.dmi'

/obj/multiz/ladder
	name = "ladder"
	icon_state = "ladderdown"
	alt = "ladderup"

/obj/multiz/ladder/metal
	icon_state = "tladderdown"
	alt = "tladderup"

/obj/multiz/stairs
	name = "dirt stairs"
	icon_state = "caveramp"

/obj/multiz/stairs/wood
	name = "wooden stairs"
	icon_state = "caveramp"

/obj/multiz/stairs/metal
	name = "metal stairs"
	icon_state = "caveramp"
