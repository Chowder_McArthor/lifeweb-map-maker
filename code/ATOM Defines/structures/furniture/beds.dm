/obj/structure/bed
	name = "bed"
	icon = 'icons/structures/furniture/beds.dmi'
	icon_state = "bed"

/obj/structure/bed/wooden
	name = "wooden bed"
	icon_state = "wood"

/obj/structure/bed/metal
	name = "metal bed"
	icon_state = "metal"

/obj/structure/bed/cave
	name = "cave bed"
	icon_state = "cave"

/obj/structure/bed/legacy
	name = "legacy bed"
	icon_state = "legacy"

/obj/structure/bed/bunk
	name = "bunk bed"
	icon_state = "bunk"

/obj/structure/bed/child
	name = "child bed"
	icon_state = "child"

/obj/structure/bed/prisoner
	name = "prisoner's cot"
	icon_state = "prisoner"

/obj/structure/bed/sleepingbag
	name = "sleeping bag"
	icon_state = "sleepingbag"

/obj/structure/bed/stretcher
	name = "stretcher"
	icon_state = "stretcher"

/obj/structure/bed/stretcher/space
	name = "space stretcher"
	icon_state = "spacestretcher"

/obj/structure/bed/big
	name = "big bed"
	icon = 'icons/structures/furniture/bigbed.dmi'
	icon_state = "bigbed"