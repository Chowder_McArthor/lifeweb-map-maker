/obj/structure/chair
	name = "chair"
	icon = 'icons/structures/furniture/chairs.dmi'
	icon_state = "comf_chair"
	rotate = 1

/obj/structure/chair/wood
	name = "wooden chair"
	icon_state = "wood_chair"

/obj/structure/chair/wood/fancy
	icon_state = "wooden_chair_alt"

/obj/structure/chair/wood/cave
	icon_state = "wooden_chair"
/obj/structure/chair/judge
	name = "judge's throne"
	icon_state = "judge"

/obj/structure/chair/soft
	name = "comfy chair"
	icon_state = "comfychair_black"

/obj/structure/chair/soft/blue
	name = "blue comfy chair"
	icon_state = "comfychair_teal"

/obj/structure/chair/soft/yellow
	name = "yellow comfy chair"
	icon_state = "comfychair_lime"

/obj/structure/chair/soft/brown
	name = "brown comfy chair"
	icon_state = "comfychair_brown"

/obj/structure/chair/couch/fancy
	name = "fancy couch"
	icon = 'icons/structures/furniture/couch.dmi'
	icon_state = "couch"

/obj/structure/chair/command
	name = "commander's chair"
	icon_state = "comm"

/obj/structure/chair/electric
	name = "electric chair"
	icon_state = "comfydeath"

/obj/structure/chair/wheelchair
	name = "wheelchair"
	icon_state = "wchair"

/obj/structure/chair/bench
	name = "wooden bench"
	icon_state = "capchair"

/obj/structure/chair/bench/large
	name = "wooden bench"
	icon_state = "capchair_center"

/obj/structure/chair/bench/large/right
	icon_state = "capchair_right"

/obj/structure/chair/bench/large/left
	icon_state = "capchair_left"

/obj/structure/chair/couch
	name = "old couch"
	icon_state = "couch_m"

/obj/structure/chair/couch/right
	icon_state = "couch_r"

/obj/structure/chair/couch/left
	icon_state = "couch_l"

/obj/structure/chair/cavethrone
	name = "cave throne"
	icon_state = "cave_throne"

/obj/structure/chair/church
	name = "church bench"
	icon_state = "church_center"

/obj/structure/chair/church/right
	icon_state = "church_right"

/obj/structure/chair/church/left
	icon_state = "church_left"

/obj/structure/chair/stool
	name = "stool"
	icon_state = "stool2"

/obj/structure/chair/stool/bar
	icon_state = "barstool"

/obj/structure/chair/stool/space
	icon_state = "barstool_luna"

/obj/structure/chair/stool/wooden
	name = "wooden stool"
	icon_state = "wstool"



