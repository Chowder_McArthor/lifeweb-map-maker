/obj/structure/grille
	name = "grille"
	icon = 'icons/structures/grilles.dmi'
	icon_state = "grilley"

/obj/structure/grille/fence
	name = "cemetery fence"
	icon_state = "cemetery"

/obj/structure/grille/bars
	name = "bars"
	icon_state = "bars"

/obj/structure/grille/legacy
	name = "legacy grille"
	icon_state = "legacy"