/obj/structure/blacksmith
	icon = 'icons/structures/blacksmith/hightech.dmi'

/obj/structure/blacksmith/anvil
	name = "anvil"
	icon_state = "anvil"

/obj/structure/blacksmith/cave_anvil
	name = "cave anvil"
	icon_state = "caveanvil"

/obj/structure/blacksmith/smelter
	name = "smelter"
	icon_state = "cavesmelter"

/obj/structure/blacksmith/bigsmelter
	name = "smelter"
	icon = 'icons/structures/blacksmith/smelter.dmi'
	icon_state = "smelter"
	bound_x = 96
	bound_y = 64
	pixel_x = -32
/obj/structure/blacksmith/forge
	name = "forge"
	icon = 'icons/structures/blacksmith/forge.dmi'
	icon_state = "forge1"
	pixel_x = -1