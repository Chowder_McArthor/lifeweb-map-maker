/obj/structure/door
	icon = 'icons/structures/doors.dmi'


/obj/structure/door/wooden
	name = "wooden door"
	icon_state = "wooden_door"
	density = 1

/obj/structure/door/iron
	name = "iron door"
	icon_state = "greendoor"

/obj/structure/door/metal
	name = "metal door"
	icon_state = "metaldoor"

/obj/structure/door/half
	name = "half door"
	icon_state = "halfdoor"

/obj/structure/door/airlock
	name = "airlock"
	icon_state = "generaldoor"

/obj/structure/door/airlock/quad
	name = "maintenance door"
	icon_state = "quaddoor"

/obj/structure/door/airlock/maint
	name = "maintenance door"
	icon_state = "maintdoor"

/obj/structure/door/airlock/glass
	name = "glass airlock"
	icon_state = "glassdoor"

/obj/structure/door/airlock/shutters
	name = "airlock"
	icon_state = "shutters"

/obj/structure/door/airlock/pod
	name = "pod door"
	icon_state = "pod"

/obj/structure/door/airlock/firedoor
	name = "firelock"
	icon_state = "firelock"

/obj/structure/door/airlock/jail
	name = "jail door"
	icon_state = "jail"

/obj/structure/door/airlock/leviathan
	name = "leviathan airlock"
	icon_state = "leviathan"

/obj/structure/door/airlock/water
	name = "green airlock"
	icon_state = "water"

/obj/structure/door/airlock/engineering
	name = "engineering airlock"
	icon_state = "engineering"

/obj/structure/door/airlock/medical
	name = "medical airlock"
	icon_state = "medical"

/obj/structure/door/airlock/double
	name = "double airlock"
	icon = 'icons/structures/door2.dmi'
	icon_state = "double"

/obj/structure/door/airlock/double/church
	name = "church airlock"
	icon_state = "church"