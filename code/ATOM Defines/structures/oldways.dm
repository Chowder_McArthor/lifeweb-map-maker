/obj/structure/oldways
	icon = 'icons/structures/oldways/statues.dmi'

/obj/structure/oldways/veles
	name = "Veles"
	icon_state = "VELES"

/obj/structure/oldways/thoth
	name = "Thoth"
	icon_state = "THOTH"

/obj/structure/oldways/baccus
	name = "Baccus"
	icon_state = "BACCUS"

/obj/structure/oldways/armok
	name = "Armok"
	icon_state = "ARMOK"

/obj/structure/oldways/lir
	name = "Lir"
	icon_state = "LIR"

/obj/structure/oldways/eusoh
	name = "Eusoh"
	icon_state = "EUSOH"

/obj/structure/oldways/xom
	name = "Xom"
	icon_state = "XOM"

/obj/structure/oldways/grost
	name = "Grosth"
	icon_state = "GROST"

/obj/structure/oldways/erista
	name = "Erista"
	icon_state = "ERISTA"

/obj/structure/oldways/saint
	name = "Saint"
	icon_state = "SAINT"

/obj/structure/oldways/lady
	name = "Lady"
	icon_state = "LADY"

/obj/structure/oldways/warlockaltar
	name = "warlock's altar"
	icon = 'icons/structures/oldways/altar.dmi'
	icon_state = "altar"
