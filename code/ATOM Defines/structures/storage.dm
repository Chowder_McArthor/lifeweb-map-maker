/////////////////////////////
////////Crates & Such////////
/////////////////////////////

/obj/structure/storage
	name = "container"
	icon_state = "stuffbox"
	icon = 'icons/structures/furniture/storage.dmi'

/obj/structure/storage/container
	name = "shipping container"
	icon_state = "cargo1"
	alt = "cargo0"

/obj/structure/storage/crate
	name = "crate"
	icon_state = "crate"
	alt = "crateopen"

/obj/structure/storage/crate/green
	icon_state = "ecrate"
	alt = "ecrateopen"

/obj/structure/storage/crate/freezer
	name = "freezer"
	icon_state = "freezer"
	alt = "freezeropen"

/obj/structure/storage/crate/tnc
	name = "ancient crate"
	icon_state = "ancient"
	alt = "ancientopen"

/obj/structure/storage/chest
	name = "wooden chest"
	icon_state = "gold"
	alt = "goldopen"

/obj/structure/storage/chest/b
	icon_state = "bgold"
	alt = "bgoldopen"

/obj/structure/storage/chest/c
	icon_state = "cgold"
	alt = "cgoldopen"

/obj/structure/storage/chest/d
	icon_state = "dgold"
	alt = "dgoldopen"

/obj/structure/storage/mining
	name = "mining cart"
	icon_state = "miningcar"

/obj/structure/storage/mining/wooden
	name = "wooden mining cart"
	icon_state = "wminingcar"


/////////////////////////////
//////Closets & Lockers//////
/////////////////////////////


/obj/structure/storage/closet
	name = "locker"
	icon = 'icons/structures/furniture/closets.dmi'
	icon_state = "closed"
	alt = "open"

/obj/structure/storage/closet/tnc
	name = "/improper TNC locker"
	icon_state = "nt"
	alt = "ntopen"

/obj/structure/storage/closet/tnc/b
	icon_state = "tnc2"
	alt = "tnc2open"

/obj/structure/storage/closet/tnc/c
	icon_state = "tnc2b"
	alt = "tnc2bopen"

/obj/structure/storage/closet/tnc/d
	icon_state = "tnc"
	alt = "tncopen"

/obj/structure/storage/closet/tribunal
	name = "tribunal suit storage unit"
	icon_state = "black"
	alt = "blackopen"

/obj/structure/storage/closet/suit
	name = "suit storage unit"
	icon_state = "ssuit"
	alt = "ssuitopen"

/obj/structure/storage/closet/bio
	name = "biohazard locker"
	icon_state = "bio"
	alt = "bioopen"

/obj/structure/storage/closet/emergency
	name = "emergency locker"
	icon_state = "bio"
	alt = "bioopen"

/obj/structure/storage/closet/fridge
	name = "fridge"
	icon_state = "fridge1"
	alt = "fridgeopen"

/obj/structure/storage/closet/green
	name = "green locker"
	icon_state = "portal"
	alt = "portalopen"

/obj/structure/storage/closet/brown
	name = "brown locker"
	icon_state = "mining"
	alt = "miningopen"

/obj/structure/storage/closet/inquisition
	name = "inquisition locker"
	icon_state = "inqsec"
	alt = "inqsecopen"

/obj/structure/storage/closet/dim
	name = "dim locker"
	icon_state = "dim"
	alt = "dimopen"

/obj/structure/storage/closet/ceberus
	name = "ceberus locker"
	icon_state = "cerb1"
	alt = "cerbo"


////////////////////////////////////////
////////////////Cabinets////////////////
////////////////////////////////////////

/obj/structure/storage/closet/cabinet
	name = "cabinet"
	icon = 'icons/structures/furniture/cabinets.dmi'
	icon_state = "cabinet1_closed"
	alt = "cabinet1_open"

/obj/structure/storage/closet/cabinet/round
	name = "rounded cabinet"
	icon_state = "cabinet2_closed"
	alt = "cabinet2_open"

/obj/structure/storage/closet/cabinet/crap
	name = "poor cabinet"
	icon_state = "cabinet0_closed"
	alt = "cabinet0_open"

/obj/structure/storage/closet/cabinet/fancy
	name = "fancy cabinet"
	icon_state = "cabinet_closed"
	alt = "cabinet_open"

/obj/structure/storage/closet/cabinet/cave
	name = "cave cabinet"
	icon_state = "wooden"
	alt = "woodenopen"

/obj/structure/storage/closet/cabinet/witch
	name = "witch's cabinet"
	icon_state = "witchcloset"
	alt = "witchcloset_open"

/obj/structure/storage/closet/cabinet/fat
	name = "fat cabinet"
	icon_state = "fat"
	alt = "fatopen"

/obj/structure/storage/closet/cabinet/coffin
	name = "coffin"
	icon_state = "coffin"
	alt = "coffin_open"

/obj/structure/storage/closet/cabinet/coffin/wood
	name = "wooden coffin"
	icon_state = "woodencoffin"
	alt = "woodencoffin_open"
